from unittest import TestCase
import os
from collections import OrderedDict
from bitarray import bitarray
from fastadist.fastadist_functions import (
    create_sequence_bitarrays, 
    create_all_sequence_bitarrays,
    calculate_all_distances,
    calculate_alignment_distances,
    write_distances_to_file,
    create_sequence_bitarrays_linked_to_id,
    calculate_distance_linked_to_two_seq_ids,
    get_tip_order_from_tree
)

import tempfile
from Bio import SeqIO

class TestDistanceMatrixCreation(TestCase):

    def test_encoding(self):
        # upper case all Gs
        self.assertEqual(
            {
                'G': bitarray('111'),
                'A': bitarray('000'),
                'T': bitarray('000'),
                'C': bitarray('000'),
                '-': bitarray('000'),
                'N': bitarray('000')
            },
            create_sequence_bitarrays('GGG')
        )
        # lower case all Gs
        self.assertEqual(
            {
                'G': bitarray('111'),
                'A': bitarray('000'),
                'T': bitarray('000'),
                'C': bitarray('000'),
                '-': bitarray('000'),
                'N': bitarray('000')
            },
            create_sequence_bitarrays('ggg')
        )
        # test all bases
        self.assertEqual(
            {
                'G': bitarray('1000000'),
                'A': bitarray('0100000'),
                'T': bitarray('0010000'),
                'C': bitarray('0001000'),
                '-': bitarray('0000100'),
                'N': bitarray('0000011')
            },
            create_sequence_bitarrays('GATC-NX')
        )       
    
    def test_creation_of_bit_arrays_from_msa(self):
        alignment_filepath = '{0}/test_data/test_align1.fas'.format(os.path.dirname(os.path.realpath(__file__)))
        self.assertEqual(
            OrderedDict([
                ('seq1', {
                    'G': bitarray('100'),
                    'A': bitarray('010'),
                    'T': bitarray('001'),
                    'C': bitarray('000'),
                    '-': bitarray('000'),
                    'N': bitarray('000')
                }), 
                ('seq2', {
                    'G': bitarray('100'),
                    'A': bitarray('000'),
                    'T': bitarray('000'),
                    'C': bitarray('001'),
                    '-': bitarray('010'),
                    'N': bitarray('000')
                }),
                ('seq3', {
                    'G': bitarray('000'),
                    'A': bitarray('101'),
                    'T': bitarray('000'),
                    'C': bitarray('000'),
                    '-': bitarray('000'),
                    'N': bitarray('010')
                }),
                ('seq4', {
                    'G': bitarray('100'),
                    'A': bitarray('001'),
                    'T': bitarray('000'),
                    'C': bitarray('010'),
                    '-': bitarray('000'),
                    'N': bitarray('000')
                })
            ]),
            create_all_sequence_bitarrays(alignment_filepath)
        )
    
    def test_creation_of_distances_from_msa(self):
        alignment_filepath = '{0}/test_data/test_align1.fas'.format(os.path.dirname(os.path.realpath(__file__)))
        distances = create_all_sequence_bitarrays(alignment_filepath)
        self.assertEqual(
            {
                'seq1': {
                    'seq2': 1,
                    'seq3': 2,
                    'seq4': 2
                },
                'seq2': {
                    'seq3': 2,
                    'seq4': 1
                },
                'seq3': {
                    'seq4': 1
                }
            },
            calculate_all_distances(distances)
        )

        alignment_filepath = '{0}/test_data/test_align2.fas'.format(os.path.dirname(os.path.realpath(__file__)))
        distances = create_all_sequence_bitarrays(alignment_filepath)
        self.assertEqual(
            {
                'seq1': {
                    'seq2': 2,
                    'seq3': 5,
                },
                'seq2': {
                    'seq3': 6
                }
            },
            calculate_all_distances(distances)
        )

    def test_write_distance_matrix_to_file(self):
        sequence_ids = ['seq1', 'seq2', 'seq3']
        distances = {
            'seq1': {
                'seq2': 5,
                'seq3': 4
            },
            'seq2': {
                'seq3': 2
            }
        }
        with tempfile.NamedTemporaryFile() as output:
            write_distances_to_file(sequence_ids, distances, output.name)
            self.assertEqual(
                '\tseq1\tseq2\tseq3\nseq1\t0\t5\t4\nseq2\t5\t0\t2\nseq3\t4\t2\t0\n',
                output.read().decode("utf-8")
            )

    def test_tip_label_retrieval(self):
        tree_filepath = '{0}/test_data/tree.nwk'.format(os.path.dirname(os.path.realpath(__file__)))       
        self.assertEqual(['seq2', 'seq4', 'seq1', 'seq3'], get_tip_order_from_tree(tree_filepath))
    

    def test_creation_of_distance_matrix_from_msa(self):
        alignment_filepath = '{0}/test_data/test_align1.fas'.format(os.path.dirname(os.path.realpath(__file__)))
        # test tsv format
        with tempfile.NamedTemporaryFile() as output:
            calculate_alignment_distances(alignment_filepath, output.name, 'tsv')
            self.assertEqual(
                '\tseq1\tseq2\tseq3\tseq4\nseq1\t0\t1\t2\t2\nseq2\t1\t0\t2\t1\nseq3\t2\t2\t0\t1\nseq4\t2\t1\t1\t0\n',
                output.read().decode("utf-8") 
            )
            print(output.read())
        
        # test tsv format with order specified by tree
        tree_filepath = '{0}/test_data/tree.nwk'.format(os.path.dirname(os.path.realpath(__file__)))
        with tempfile.NamedTemporaryFile() as output:
            calculate_alignment_distances(alignment_filepath, output.name, 'tsv', tree_filepath=tree_filepath)
            self.assertEqual(
                '\tseq2\tseq4\tseq1\tseq3\nseq2\t0\t1\t1\t2\nseq4\t1\t0\t2\t1\nseq1\t1\t2\t0\t2\nseq3\t2\t1\t2\t0\n',
                output.read().decode("utf-8") 
            )
            print(output.read())
        
        # test csv format
        with tempfile.NamedTemporaryFile() as output:
            calculate_alignment_distances(alignment_filepath, output.name, 'csv')
            self.assertEqual(
                ',seq1,seq2,seq3,seq4\nseq1,0,1,2,2\nseq2,1,0,2,1\nseq3,2,2,0,1\nseq4,2,1,1,0\n',
                output.read().decode("utf-8") 
            )
            print(output.read())

        # test phylip format
        with tempfile.NamedTemporaryFile() as output:
            calculate_alignment_distances(alignment_filepath, output.name, 'phylip')
            self.assertEqual(
                '4\nseq1\t0\t1\t2\t2\nseq2\t1\t0\t2\t1\nseq3\t2\t2\t0\t1\nseq4\t2\t1\t1\t0\n',
                output.read().decode("utf-8") 
            )
            print(output.read())
    
    def test_tuples_for_parallel_processes(self):
        #test creation of bitarray
        sequence_filepath = '{0}/test_data/seq1.fas'.format(os.path.dirname(os.path.realpath(__file__)))
        sequence_record = SeqIO.read(sequence_filepath, 'fasta')
        self.assertEqual(
            ('seq1',
                {
                'G': bitarray('1000'),
                'A': bitarray('0100'),
                'T': bitarray('0010'),
                'C': bitarray('0001'),
                '-': bitarray('0000'),
                'N': bitarray('0000')
                }
            ),
            create_sequence_bitarrays_linked_to_id(sequence_record)
        )

        #test creation of distances
        seq_id1 = 'seq1'
        seq_id2 = 'seq2'
        seq1_bitarrays = {
                'G': bitarray('1000'),
                'A': bitarray('0100'),
                'T': bitarray('0010'),
                'C': bitarray('0001'),
                '-': bitarray('0000'),
                'N': bitarray('0000')
            }
        seq2_bitarrays = {
                'G': bitarray('1100'),
                'A': bitarray('0000'),
                'T': bitarray('0010'),
                'C': bitarray('0001'),
                '-': bitarray('0000'),
                'N': bitarray('0000')
            }
        self.assertEqual(
            ('seq1', 'seq2', 1),
            calculate_distance_linked_to_two_seq_ids(seq_id1, seq_id2, seq1_bitarrays, seq2_bitarrays)
        )
        